// 业务模块选项
const businessOptions = {
  sysNormalDisableOption:[
    { label: '正常', value: '0' },
    { label: '停用', value: '1' },
  ],
  sysUserSexOption:[
    { label: '男', value: '0' },
    { label: '女', value: '1' },
    { label: '未知', value: '2' },
  ],
  sysShowHideOption:[
    { label: '显示', value: '0' },
    { label: '隐藏', value: '1' },
  ],
  selectLabel:(option,value)=>{
    return option.filter(x=>x.value === value)[0]?option.filter(x=>x.value === value)[0].label:'';
  }
}

export default Object.assign({}, businessOptions)
