import { login, logout, getInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    id: '',
    name: '',
    avatar: '',
    roles: [],
    permissions: []
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_ID: (state, id) => {
      state.id = id
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim()
      const password = userInfo.password
      const code = userInfo.code
      const uuid = userInfo.uuid
      return new Promise((resolve, reject) => {
        // login(username, password, code, uuid).then(res => {
        //   setToken(res.token)
        //   commit('SET_TOKEN', res.token)
        //   resolve()
        // }).catch(error => {
        //   reject(error)
        // })


        console.log('登录',username, password, code, uuid)
        let res = {
          "msg": "操作成功",
          "code": 200,
          "token": "eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjExODEyYWFmLWI2NDUtNDVmMi05MDc5LTBhYjFhMTBlZTlhZiJ9.rjeYL_XFolEPQGeWSkylK57-WVPrLG_ZmRx5x1J7gFhPzp7rM-3-Ob1ivypQ-ohAkxwIC-J4eumOCfWFh_uJjg"
        }
        setToken(res.token)
        commit('SET_TOKEN', res.token)
        resolve()

      })
    },

    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        // getInfo().then(res => {
        //   const user = res.user
        //   const avatar = (user.avatar == "" || user.avatar == null) ? require("@/assets/images/profile.jpg") : process.env.VUE_APP_MINIO_SERVER + user.avatar;
        //   if (res.roles && res.roles.length > 0) { // 验证返回的roles是否是一个非空数组
        //     commit('SET_ROLES', res.roles)
        //     commit('SET_PERMISSIONS', res.permissions)
        //   } else {
        //     commit('SET_ROLES', ['ROLE_DEFAULT'])
        //   }
        //   commit('SET_ID', user.userId)
        //   commit('SET_NAME', user.userName)
        //   commit('SET_AVATAR', avatar)
        //   resolve(res)
        // }).catch(error => {
        //   reject(error)
        // })

        console.log('获取用户信息')
        let res = {
          "msg": "操作成功",
          "code": 200,
          "permissions": [
            "system:user:resetPwd",
            "system:post:list",
            "system:user:remove",
            "system:menu:query",
            "system:dept:remove",
            "system:menu:list",
            "system:menu:add",
            "system:role:list",
            "system:user:import",
            "system:user:edit",
            "system:user:query",
            "system:post:export",
            "system:role:edit",
            "system:user:add",
            "system:dept:list",
            "system:user:export",
            "system:role:remove",
            "system:role:add",
            "system:menu:remove",
            "system:dept:query",
            "system:dept:edit",
            "system:post:add",
            "system:user:list",
            "system:role:export",
            "system:post:edit",
            "system:role:query",
            "system:post:query",
            "system:post:remove",
            "system:menu:edit",
            "system:dept:add"
          ],
          "roles": [
            "common"
          ],
          "user": {
            "userId": 2,
            "userName": "ry",
            "avatar": "/it/u=1956604245,3662848045&fm=193&f=GIF"
          }
        }
        const user = res.user
        const avatar = (user.avatar == "" || user.avatar == null) ? require("@/assets/images/profile.jpg") : process.env.VUE_APP_MINIO_SERVER + user.avatar;
        if (res.roles && res.roles.length > 0) { // 验证返回的roles是否是一个非空数组
          commit('SET_ROLES', res.roles)
          commit('SET_PERMISSIONS', res.permissions)
        } else {
          commit('SET_ROLES', ['ROLE_DEFAULT'])
        }
        commit('SET_ID', user.userId)
        commit('SET_NAME', user.userName)
        commit('SET_AVATAR', avatar)
        resolve(res)

      })
    },

    // 退出系统
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        // logout(state.token).then(() => {
        //   commit('SET_TOKEN', '')
        //   commit('SET_ROLES', [])
        //   commit('SET_PERMISSIONS', [])
        //   removeToken()
        //   resolve()
        // }).catch(error => {
        //   reject(error)
        // })
        console.log("退出登录", state.token);
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        commit('SET_PERMISSIONS', [])
        removeToken()
        resolve()
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
